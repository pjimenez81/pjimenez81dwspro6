<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Teléfonos</title>
        <meta charset="UTF-8">
    </head>
    <body >
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . '/pjimenez81dwspro6/controlador/Funciones.php');
        include_once($_SERVER['DOCUMENT_ROOT'] . '/pjimenez81dwspro6/controlador/ControladorTelefono.php');
        include_once($_SERVER['DOCUMENT_ROOT'] . '/pjimenez81dwspro6/controlador/ControladorPersona.php');
        $telefono = new ControladorTelefono();
        $persona = new ControladorPersona();
        cabecera();
        ?>

        <h1>Alta Teléfonos</h1>
        <form method="POST" action="../controlador/ControladorGrabarTelefono.php" >
            <table>
                <tr>
                    <td>Id:</td><td><input type="text" name="id" value="<?php $telefono->calcularIDTelefonos() ?>"/></td>
                </tr>
                <tr>
                    <td>Numero:</td><td><input type="text" name="numero" /></td>
                </tr>

                <tr>
                    <th>Id Persona</th>
                    <td><select name="idpersona">
                            <option value ="0"> Elije Propietario del Teléfono </option>
                            <?php
                            $persona->comboBox();
                            ?>
                        </select></td>


                </tr>
                <tr>
                    <td><button type="submit">Guardar</button></td>
                </tr>


                <?php
                $telefono->rellenarTablaTelefono();
                ?>

            </table>
        </form>



        <a href="VistaPrincipal.php">Volver a la Vista Principal</a><br>
        <?php inicio(); ?>
        <?php pie(); ?>

    </body>
</html>