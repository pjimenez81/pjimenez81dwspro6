<?php

interface Modelo {
    //persona
    public function crearPersona($persona);
    public function leerPersona();
    public function idPersona();
    
    //telefono
    public function crearTelefono($telefono);
    public function leerTelefono();
    public function idTelefono();
    
}
