<?php

include_once($_SERVER['DOCUMENT_ROOT'] . '/pjimenez81dwspro6/modelo/Persona.php');
include_once($_SERVER['DOCUMENT_ROOT'] . '/pjimenez81dwspro6/modelo/Telefono.php');
include_once($_SERVER['DOCUMENT_ROOT'] . '/pjimenez81dwspro6/modelo/Modelo.php');

class ModeloFicheros implements Modelo {

    private $fpersonas = "../BdFicheros/personas.csv";
    private $ftelefonos = "../BdFicheros/telefonos.csv";
    
    public function __construct() {
	if (!file_exists("../BdFicheros/personas.csv")) {
	    $archivo = fopen("../BdFicheros/personas.csv", "w");
	    fclose($archivo);
	};
	if (!file_exists("../BdFicheros/telefonos.csv")) {
	    $archivo = fopen("../BdFicheros/telefonos.csv", "w");
	    fclose($archivo);
	};
    }
    

    public function crearPersona($persona) {
	$f = fopen($this->fpersonas, "a");
	$linea = $persona->__GET('id') . ";"
		. $persona->__GET('nombre') . "\r\n ";
	fwrite($f, $linea);
	fclose($f);
    }

    public function leerPersona() {
	$personas = array();

	if ($archivo = fopen($this->fpersonas, "r")) {
	    $token = fgetcsv($archivo, 0, ";"); 
	    while ($token) {
		$persona = new Persona($token[0], $token[1], $token[2]);
		array_push($personas, $persona);
		$token = fgetcsv($archivo, 0, ";");
	    }
	    fclose($archivo);
	} else {
	    echo "Error";
	}

	return $personas;
    }

    public function crearTelefono($telefono) {
	$f = fopen($this->ftelefonos, "a");
	$linea = $telefono->__GET('id') . ";"
		. $telefono->__GET('nombre') . ";"
		. $telefono->__GET('idpersona') . "\r\n";
	fwrite($f, $linea);
	fclose($f);
    }

    public function leerTelefono() {
	$perros = array();

	if ($archivo = fopen($this->ftelefonos, "r")) {
	    $token = fgetcsv($archivo, 0, ";"); 
	    while ($token) {
		$persona = new Persona($token[4],null, null);
		$telefono = new Telefono($token[0], $token[1], $persona);
		array_push($telefonos, $telefono);
		
		$token = fgetcsv($archivo, 0, ";");
	    }
	    fclose($archivo);
	} else {
	    
	}

	return $telefonos;
    }

    public function idPersona() {
	$modelo = new ModeloFicheros();
	$personas = $modelo->leerPersona();
	$ultima = end($personas);
	$ultimoID = $ultima->__GET('id');
	$ultimoID++;
	echo $ultimoID;
    }

    public function idTelefono() {
	$modelo = new ModeloFicheros();
	$telefonos = $modelo->leerTelefono();
	$ultimo = end($telefonos);
	$ultimoID = $ultimo->__GET('id');
	$ultimoID++;
	echo $ultimoID;
    }

}
