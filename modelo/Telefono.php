<?php


class Telefono {
    private $id;
    private $numero;
    private $idpersona;
    
    
    public function __CONSTRUCT($id, $numero, $idpersona) {
	$this->id = $id;
	$this->numero = $numero;
	$this->idpersona = $idpersona;
        
    }
    
    public function __GET($k) {
	return $this->$k;
    }

    public function __SET($k, $v) {
	return $this->$k = $v;
    }
}
